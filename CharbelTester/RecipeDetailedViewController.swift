//
//  RecipeDetailedViewController.swift
//  CharbelTester
//
//  Created by Georges on 9/28/21.
//

import Foundation
import UIKit
import SwiftyJSON
import GoogleMobileAds

struct RecipeData {
    static var data = [String:Any]()
}

class RecipeDetailedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    @IBOutlet weak var tableView: UITableView!
    var ingredients = [String]()
    var steps = [String]()
    var bannerView: GADBannerView!
    var removeAdsButton: UIButton?
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        overrideUserInterfaceStyle = .light
        
        let ingredientsJSON = RecipeData.data["ingredients"] as! [JSON]
        
        for ing in ingredientsJSON {
            ingredients.append("\(ing)")
        }
        
        let stepsJSON = RecipeData.data["steps"] as! [JSON]
        
        for step in stepsJSON {
            steps.append("\(step)")
        }
        
        tableView.reloadData()
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        
        GlobalData.willShowIntAd = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !GlobalData.willShowAds {
            navigationItem.rightBarButtonItem = nil
            bannerView.removeFromSuperview()
        }
    }
    
    @IBAction func removeAddsTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: "Remove Ads", message: "Pay USD 2.99 to remove ads.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Purchase", style: .default, handler: {_ in
            GlobalData.willShowAds = false
            self.viewWillAppear(false)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
          [NSLayoutConstraint(item: bannerView,
                              attribute: .bottom,
                              relatedBy: .equal,
                              toItem: self.tableView,
                              attribute: .bottom,
                              multiplier: 1,
                              constant: -20),
           NSLayoutConstraint(item: bannerView,
                              attribute: .centerX,
                              relatedBy: .equal,
                              toItem: view,
                              attribute: .centerX,
                              multiplier: 1,
                              constant: 0)
          ])
        
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
         bannerView.rootViewController = self
         bannerView.load(GADRequest())
        
        
        
        
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //Main Image Cell
        if section == 0 {
            return 1
        }
        
        //Ingredients
        if section == 1 {
            return ingredients.count
        }
        
        //Steps
        if section == 2 {
            return steps.count
        }
        
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Main Image Cell
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "mainImageCell", for: indexPath) as! MainImageCell
            let urlSTR = RecipeData.data["imageURL"] as! String
            cell.setup(urlSTR: urlSTR)
            return cell
        }
        //Ingredients
        if indexPath.section == 1 {
            let ingredient = ingredients[indexPath.row]
            let cell = UITableViewCell()
            cell.textLabel?.text = "- \(ingredient)"
            cell.textLabel?.font = UIFont(name: "Avenir", size: 17)!
            return cell
        }
        //Steps
        if indexPath.section == 2 {
          
            let cell = tableView.dequeueReusableCell(withIdentifier: "stepCell", for: indexPath) as! StepCell
            cell.setup(step: steps[indexPath.row], row: indexPath.row)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
}

class MainImageCell : UITableViewCell {
    
    @IBOutlet weak var foodImageView: UIImageView!
    
    func setup(urlSTR: String) {
        let urlSTREncoded = urlSTR.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let url = URL(string: urlSTREncoded)
        foodImageView.kf.setImage(with: url)
    }
    
    
}

class StepCell : UITableViewCell {
    
    @IBOutlet weak var stepNumberLabel: UILabel!
    @IBOutlet weak var stepLabel: UILabel!
    
    func setup(step: String, row: Int) {
        stepNumberLabel.text = "Step \(row + 1)"
        stepLabel.text = step
    }
    
}



