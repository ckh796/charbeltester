//
//  ViewController.swift
//  CharbelTester
//
//  Created by Georges on 9/28/21.
//

import UIKit
import SwiftyJSON
import Kingfisher
import GoogleMobileAds

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let jsonURL = "http://testtask.solidtechapps.com/api/v1/response/"
    var numberOfSections = 0
    var bannerView: GADBannerView!
    var allRecipesDataDictionary = [String:[[String:Any]]]()
    var removeAdsButton: UIButton?
    private var interstitial: GADInterstitialAd?
    var firstLoad = true
    var didAlreadyLoadInterstitialAd = false
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        overrideUserInterfaceStyle = .light
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 70, right: 0)
        
        loadJson(fromURLString: jsonURL)
        
        
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        addBannerViewToView(bannerView)
        
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !GlobalData.willShowAds {
            navigationItem.rightBarButtonItem = nil
            bannerView.removeFromSuperview()
            return
        }
        
        if GlobalData.willShowIntAd {
            let request = GADRequest()
                GADInterstitialAd.load(withAdUnitID:"ca-app-pub-3940256099942544/4411468910",
                                            request: request,
                                  completionHandler: { [self] ad, error in
                                    if let error = error {
                                      print("Failed to load interstitial ad with error: \(error.localizedDescription)")
                                      return
                                    }
                                    interstitial = ad
                                    if self.interstitial != nil {
                                        self.interstitial!.present(fromRootViewController: self)
                                        GlobalData.willShowIntAd = false
                                    }
                                    
                                  }
                )
        }
        
    }
    
  
    
    @IBAction func removeAddsTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: "Remove Ads", message: "Pay USD 2.99 to remove ads.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Purchase", style: .default, handler: {_ in
            GlobalData.willShowAds = false
            self.viewWillAppear(false)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        
        
        
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
          [NSLayoutConstraint(item: bannerView,
                              attribute: .bottom,
                              relatedBy: .equal,
                              toItem: self.collectionView,
                              attribute: .bottom,
                              multiplier: 1,
                              constant: -20),
           NSLayoutConstraint(item: bannerView,
                              attribute: .centerX,
                              relatedBy: .equal,
                              toItem: view,
                              attribute: .centerX,
                              multiplier: 1,
                              constant: 0)
          ])
        
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
         bannerView.rootViewController = self
         bannerView.load(GADRequest())
        
       
        
       }
    
    private func loadJson(fromURLString urlString: String) {
        if let url = URL(string: urlString) {
            let urlSession = URLSession(configuration: .default).dataTask(with: url) { (data, response, error) in
                if let error = error {
                    print(error.localizedDescription)
                }
                
                if let data = data {
                    DispatchQueue.main.async {
                        self.parseJson(data: data)
                    }
                    
                }
            }
            
            urlSession.resume()
        }
    }
    
    func parseJson(data: Data)  {
        
        let json = JSON(data)
        let allFoods = json["AllFoods"].array!
        
        var compiledData = [String:[[String:Any]]]()
        
        for (j) in allFoods {
            let foodCategoryName = j["categoryName"].string!
            let recipes = j["receipes"].array!
            
            for (j2) in recipes {
                let imageURL = j2["imageurl"].string!
                let ingredients = j2["ingredients"].array!
                let smallDescription = j2["smalldescription"].string!
                let steps = j2["steps"].array!
                let name = j2["name"].string!
                let timetoprepare = j2["timetoprepare"].string!
                
                let recipeObject : [String:Any] = [
                    "imageURL":imageURL,
                    "ingredients":ingredients,
                    "smallDescription":smallDescription,
                    "steps":steps,
                    "name":name,
                    "timetoprepare":timetoprepare
                ]
                
                
                if compiledData[foodCategoryName] == nil {
                    let emptyArrayObject = [[String:Any]]()
                    compiledData[foodCategoryName] = emptyArrayObject
                }
                
                compiledData[foodCategoryName]!.append(recipeObject)
                
            }
      
        }
        
        self.allRecipesDataDictionary = compiledData
        populateCollectionView()
    }
    
    func populateCollectionView() {
        
        //Number of sections equals number of categories
        numberOfSections = allRecipesDataDictionary.keys.count
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recipeCell", for: indexPath) as! RecipeCell
        let keys = allRecipesDataDictionary.keys.sorted()
        let category = keys[indexPath.section]
        let recipeObjects = allRecipesDataDictionary[category]!
        let data = recipeObjects[indexPath.row]
        cell.setup(data: data)
        
        return cell
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return numberOfSections
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let keys = allRecipesDataDictionary.keys.sorted()
        let key = keys[section]
        let recipeObjects = allRecipesDataDictionary[key]!
        return recipeObjects.count
     
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.width/2), height: 300 )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "recipeHeader", for: indexPath) as? RecipeSectionHeader{
            let keys = allRecipesDataDictionary.keys.sorted()
            let category = keys[indexPath.section]
            sectionHeader.sectionHeaderlabel.text = category
            return sectionHeader
        }
        return UICollectionReusableView()
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let keys = allRecipesDataDictionary.keys.sorted()
        let category = keys[indexPath.section]
        let recipeObjects = allRecipesDataDictionary[category]!
        let data = recipeObjects[indexPath.row]
        RecipeData.data = data
        self.performSegue(withIdentifier: "goToDetails", sender: nil)
    }

}


class RecipeCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var prepTimeLabel: UILabel!
    @IBOutlet weak var ingredientsLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cardBG: UIView!
    
    func setup(data: [String:Any]) {
        let name = data["name"] as! String
        nameLabel.text = name
        
        let prepTime = data["timetoprepare"] as! String
        prepTimeLabel.text = prepTime
        
        let ingredients = data["ingredients"] as! [JSON]
        var str = ""
        for ing in ingredients {
            str += "\(ing), "
        }
        ingredientsLabel.text = str
      
        cardBG.layer.cornerRadius = 16
        cardBG.layer.masksToBounds = true
        cardBG.layer.borderWidth = 1
        cardBG.layer.borderColor = UIColor.lightGray.cgColor
        
        let urlSTR = (data["imageURL"] as! String).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
      
       
        let url = URL(string: urlSTR)
        
        imageView.kf.setImage(with: url)
        
       
        
    }
    
}


class RecipeSectionHeader: UICollectionReusableView {
    @IBOutlet weak var sectionHeaderlabel: UILabel!
}
